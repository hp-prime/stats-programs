EXPORT AnovaTabl_NesL_a(NesL,a) //multilpe groups of data
BEGIN
LOCAL i:=1,j:=1,k:=1,m:=1,ct:=1,Mean_all:={},GrandM:=0;
LOCAL res_sstr:=0,res_sse:=0,numgrp:=0;
LOCAL n:=0,tot:=0,d:=0,crt:=0,res:={},fstat:=0,freq:=0,expt:=0;

WHILE i<=size(NesL) DO
   Mean_all:=append(Mean_all,EVAL(mean(NesL(i))));
   i:=i+1;
END;

WHILE ct<=size(Mean_all) DO
   expt:=expt+Mean_all(ct)*size(NesL(ct));
   freq:=freq+size(NesL(ct));
   ct:=ct+1;
END;

GrandM:=expt/freq;

WHILE j<=size(Mean_all) DO
   res_sstr:=res_sstr + (Mean_all(j)-GrandM)^2*size(NesL(j));
   j:=j+1;
END;

WHILE k <= size(NesL) DO
   WHILE m<=size(NesL(k)) DO
      tot:=tot+size(NesL(k,m));
      res_sse:=res_sse+(NesL(k,m)-Mean_all(k))^2;
      m:=m+1;
   END;
   m:=1;
   k:=k+1;
END;
n:=size(NesL)-1;
d:=tot-size(NesL);
numgrp:= size(NesL);
crt:=FISHER_ICDF(n,d,1-a);
fstat:=EVAL(res_sstr/n)/EVAL(res_sse/(freq-size(NesL)));
LOCAL mse:=0,mstrt:=0,sst:=0;
sst:=(ROUND(res_sstr,4)+ROUND(res_sse,4));
mse:=res_sse/d;
mstrt:=res_sstr/n;
res:=append(res,"F cal: "+ROUND(fstat,4));
res:=append(res,"F tabl: "+ROUND(crt,4)+" df: n,d,a: ("+n+","+d+"),"+(1-a));
res:=append(res,"SSE: " + ROUND(res_sse,4));
res:=append(res,"SSTR: " + ROUND(res_sstr,4));
res:=append(res,"SST: " + ROUND(sst,3));
res:=append(res,"mse: " + ROUND(mse,3));
res:=append(res,"mstrt: " + ROUND(mstrt,3));
res:=append(res,"num of group & total size: " + numgrp +" & " +tot);
res:=append(res,"grand mean: " + ROUND(GrandM,3));
res:=append(res,"mean of each group: " + Mean_all);

return res;
END;

EXPORT F_Stat_n_m_sd(NesL,a) //freq (n), mean and sd of each group
BEGIN
LOCAL i:=1,GrandM:=mean(NesL(2)),res_sstr:=0,res_sse:=0,res:={},n:=0,d:=0,crt:=0;
WHILE i<=size(NesL(1)) DO
   res_sstr:=res_sstr+(NesL(2,i)-GrandM)^2*NesL(1,i);
   res_sse:=res_sse+NesL(3,i)^2*(NesL(1,i)-1);
   i:=i+1; 
END;
n:=size(NesL(1))-1;
d:=ΣLIST(NesL(1))-size(NesL(1));
res:=append(res,"F cal: "+ROUND((res_sstr/n)/(res_sse/d),4));
crt:=FISHER_ICDF(n,d,1-a);
res:=append(res,"F tabl: "+ROUND(crt,4)+" df: n,d,a: "+n+","+d+","+(1-a));
res:=append(res,"SSE: " + ROUND(res_sse,4));
res:=append(res,"SSTR: " + ROUND(res_sstr,4));
res:=append(res,"grand mean: " + ROUND(GrandM,3));
res:=append(res,"num of group & total size: " + size(NesL(1)) +" & " +ΣLIST(NesL(1)));

return res;
END;


EXPORT LSD_NesL_ni_nj_a(NesL,ni,nj,a)
BEGIN
LOCAL i:=1,j:=1,k:=1,m:=1,ct:=1,Mean_all:={},GrandM:=0;
LOCAL res_sstr:=0,res_sse:=0;
LOCAL n:=0,tot:=0,d:=0,crt:=0,res:={};
LOCAL fstat:=0,freq:=0,expt:=0,mse:=0;
LOCAL lsd:=0,meandif:=0;

WHILE i<=size(NesL) DO
   Mean_all:=append(Mean_all,EVAL(mean(NesL(i))));
   i:=i+1;
END;

WHILE ct<=size(Mean_all) DO
   expt:=expt+Mean_all(ct)*size(NesL(ct));
   freq:=freq+size(NesL(ct));
   ct:=ct+1;
END;

GrandM:=expt/freq;

WHILE j<=size(Mean_all) DO
   res_sstr:=res_sstr + (Mean_all(j)-GrandM)^2*size(NesL(j));
   j:=j+1;
END;

WHILE k <= size(NesL) DO
   WHILE m<=size(NesL(k)) DO
      tot:=tot+size(NesL(k,m));
      res_sse:=res_sse+(NesL(k,m)-Mean_all(k))^2;
      m:=m+1;
   END;
   m:=1;
   k:=k+1;
END;

n:=size(NesL)-1;
d:=tot-size(NesL);
crt:=FISHER_ICDF(n,d,a);
mse:=EVAL(res_sse/(freq-size(NesL)));
lsd:= STUDENT_ICDF( d,1-(a/2))*sqrt(mse*(1/size(NesL(ni))+1/size(NesL(nj))) );
meandif:= ABS( Mean_all(ni)-Mean_all(nj) );
IF meandif >= lsd
   THEN 
      res:=append(res,"ui != uj,lsd="+ ROUND(lsd,4)+" absolute y1-y2="+ROUND(meandif,4)+">"+ROUND(lsd,3)+" T-df: "+(1-(a/2))+","+
      d +" val: "+ ROUND(STUDENT_ICDF( d,1-(a/2)),4));
      res:=append(res,"SSE: " + ROUND(res_sse,4));
      res:=append(res,"Mean of each group: " + Mean_all);
      return res;
   ELSE
      res:=append(res,"ui isqual uj,lsd="+ ROUND(lsd,4) + " y1-y2="+ROUND(meandif,4)+"<"+lsd+" T-df: "+(1-(a/2))+","
      +d+" val: "+ ROUND(STUDENT_ICDF( d,1-(a/2)),4));
      res:=append(res,"SSE: " + ROUND(res_sse,4));
      res:=append(res,"T val: "+ STUDENT_ICDF( d,1-(a/2)));
      res:=append(res,"Mean of each group: " + Mean_all);
      return res;
   END; 
END;

EXPORT RCBD_NesL_a(NesL,alpa)
BEGIN
LOCAL i:=1,j:=1,block:={},res:={},tretSum:={};
LOCAL m:=1,k:=1,sumsqr:=0,num_cunt:=0;
LOCAL tot:=0,tretMean:={},blockMean:={},blksum:={};
WHILE i <= size(NesL) DO
    tretMean:=append(tretMean,mean(NesL(i)));
    tretSum:=append(tretSum,ΣLIST(NesL(i)));
    WHILE j <= size(NesL(i)) DO
        tot:= tot + NesL(i,j);
        sumsqr:= sumsqr + (NesL(i,j))^2;
        j:=j+1;
        num_cunt:=num_cunt+1;
    END;
    j:=1;
    i:=i+1;
END;

WHILE m <= size(NesL(k)) DO
    WHILE k <=size(NesL) DO
        block:=append(block,NesL(k,m));
        k:=k+1;
    END;
    blockMean:=append(blockMean,mean(block));
    blksum:=append(blksum,ΣLIST(block));
    block:={};
    k:=1;
    m:=m+1;
END;

LOCAL v1:=1,blksumsqr:=0;
WHILE v1<=size(blksum) DO
    blksumsqr:=blksumsqr +blksum(v1)^2;
    v1:=v1+1;
END;

LOCAL v2:=1,tretsumsqr:=0;
WHILE v2<=size(tretSum) DO
    tretsumsqr:=tretsumsqr +tretSum(v2)^2;
    v2:=v2+1;
END;

LOCAL a:=size(NesL(1)),b:=size(NesL);
LOCAL SST:=ROUND((sumsqr - tot^2/num_cunt),4);
LOCAL SSTRT:=ROUND(1/a*tretsumsqr - tot^2/num_cunt,4);
LOCAL SSB:=ROUND(1/b * blksumsqr - tot^2/num_cunt,4);
LOCAL SSE:=ROUND(SST-SSTRT-SSB,4);
LOCAL dfsse:=(a-1)*(b-1);
LOCAL dfmstrt:=(b-1);
LOCAL MSB:= SSB/(a-1);
res:=append(res, "Fmstrt cal: " + ROUND((SSTRT/dfmstrt) / (SSE/dfsse),4) );
res:=append(res, "Fmstrt tab" + "(" +(1-alpa)+","+ dfmstrt +"," +dfsse +") val:"+ ROUND(FISHER_ICDF(dfmstrt,dfsse,1-alpa),4));
res:=append(res, "Fmsb cal: " + ROUND(MSB/(SSE/dfsse),4) );
res:=append(res, "Fmsb tab" + "(" +(1-alpa) +" dfmsb" +"," +(a-1) +") val:"+ ROUND(FISHER_ICDF(a-1,dfsse,1-alpa),4));
res:=append(res,"Mstrt: " + ROUND(SSTRT/dfmstrt,3) );
res:=append(res,"MSE: " + ROUND(SSE/dfsse,3) );
res:=append(res,"MSB: " + ROUND(MSB,3) );
res:=append(res,"SSE: " + SSE);
res:=append(res,"SST: " + SST );
res:=append(res,"SSTRT: " + SSTRT );
res:=append(res,"SSB: " + SSB );
res:=append(res,"trt_Vertical_sum yi: " +tretSum);
res:=append(res,"block_Horiz_sum yj: " +blksum);
res:=append(res,"Total T: "+ tot);
res:=append(res,"sum sqr all: "+ sumsqr);
res:=append(res,"Treatmean: "+ tretMean);
res:=append(res,"blockMean: "+ blockMean);
return res;

END;







