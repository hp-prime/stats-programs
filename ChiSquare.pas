EXPORT IntvlV1_s_n_a(s,n,a)
BEGIN
LOCAL res:={},Left:=0,Right:=0;
Left:=(n-1)*s^2/(CHISQUARE_ICDF(n-1,(1-a/2)));
Right:=(n-1)*s^2/(CHISQUARE_ICDF(n-1,a/2));
res:=append(res,"Variance Interval ("+ ROUND(Left,3) + "," + ROUND(Right,3)+")"); 
res:=append(res,"SD Interval ("+ ROUND(Left^0.5,3) + "," + ROUND(Right^0.5,3)+")"); 
END;

EXPORT IntvV1V2_s1s2_n1n2_a(s1,s2,n1,n2,a)
BEGIN
LOCAL res:={},Left:=0,Right:=0;
Left:=s1^2/( s2^2*FISHER_ICDF(n1-1, n2-1,(1-a/2)) );
Right:=s1^2/( s2^2*FISHER_ICDF(n1-1, n2-1,(a/2) ));
res:=append(res,"Variance Interv V1/V2 ("+ ROUND(Left,3) + "," + ROUND(Right,3)+")"); 
res:=append(res,"SD1/SD2 Interv ("+ ROUND(Left^0.5,3) + "," + ROUND(Right^0.5,3)+")"); 
END;

EXPORT VarianceTest_s_sd_n_a(s,sd,n,a)
BEGIN
LOCAL res:={},tst:=0,Ltai:=0,Rtai:=0,Twtai:={};
tst:=(n-1)*s^2/(sd^2);
Rtai:= CHISQUARE_ICDF(n-1,1-a);
Ltai:= CHISQUARE_ICDF(n-1,a);
Twtai:=append(Twtai,CHISQUARE_ICDF(n-1,(a/2)));
Twtai:=append(Twtai,CHISQUARE_ICDF(n-1,1-(a/2)));
res:=append(res,"Cal: " +ROUND(tst,3));
res:=append(res,"Left Tail tab: " +ROUND(Ltai,3));
res:=append(res,"Right Tail tab: " +ROUND(Rtai,3));
res:=append(res,"Two Tail tab: " +ROUND(Twtai,3));
return res;
END;

EXPORT FTest_s1_s2_n1_n2_a(s1,s2,n1,n2,a)
BEGIN
LOCAL ftst:=0,res:={},lef:=0,rigt:=0;
ftst:= s1^2/s2^2;
lef:=FISHER_ICDF(n1-1,n2-1,a/2);
rigt:=FISHER_ICDF(n1-1,n2-1,1-(a/2));
res:=append( res,"F cal: " +ROUND(ftst,3) );
res:=append( res,"Left tab: " +ROUND(lef,3) );
res:=append( res,"right tab: " +ROUND(rigt,3) );
return res;
END;

EXPORT ChiInpTe_L1_L2_a(L1,L2,a)
BEGIN
LOCAL i:=1,j:=1,row1Sum:=ΣLIST(L1),row2Sum:=ΣLIST(L2),N:=L1.+L2;
LOCAL E:={},E1:={},E2:={},tst:={},res:={},crt=0;
LOCAL OL:= CONCAT(L1,L2),tot:=row1Sum+row2Sum;

WHILE i<=size(N) DO
   E1:=append(E1,ROUND(row1Sum*N(i)/tot,2));
   E2:=append(E2,ROUND(row2Sum*N(i)/tot,2));
   i:=i+1;
END;

E:=CONCAT(E1,E2);
WHILE j<=size(OL) DO
   IF size(L1)-1 == 1
   THEN
      tst:=append(tst,(ABS(OL(j)-E(j))-0.5)^2/E(j));
   ELSE
      tst:=append(tst,(OL(j)-E(j))^2/E(j));
   END;
   j:=j+1;
END;

res:=append(res,ROUND(ΣLIST(tst),4));
crt:=CHISQUARE_ICDF(size(L1)-1,1-a);
IF size(L1)-1 == 1
THEN
   res:=append(EVAL(res),"Yate,df: "+"("+EVAL(size(L1)-1)+","+(1-a)+")"+" tabl=>" + ROUND(crt,4));
   res:=append(res,"Expt val chk if < 5: "+ E);
ELSE
   res:=append(EVAL(res),"2VarTest, df: "+"("+EVAL(size(L1)-1)+","+(1-a)+")"+" tabl=>" + ROUND(crt,4));
   res:=append(res,"Expt val chk if < 5: "+ E);
END;  
return res;
END;

EXPORT ChiEqualProbT_L1_a(L1,a) //Test assumption of equal probablity 
BEGIN
LOCAL i:=1,tot:=ΣLIST(L1),p1:=1/size(L1);
LOCAL tst:={},res:={},crt=0,E:={};
WHILE i<=size(L1) DO
   tst:=append(tst,(L1(i)-tot*p1)^2/(tot*p1));
   E:=append(E,ROUND(tot*p1,4));
   i:=i+1;
END;

crt:=CHISQUARE_ICDF(size(L1)-1,1-a);
res:=append(res,"cal: " +ROUND(EVAL(ΣLIST(tst)),4));
res:=append(res,"P 1 row test,df: "+"("+EVAL(size(L1)-1)+","+(1-a)+")"+" tabl=>" + ROUND(crt,4));
res:=append(res,"Expt val: "+ E);
return res;
END;

EXPORT ChiTeRatio_L1_percent_a(L1,Lratio,a) //test cases where ratio_pecentage is given
BEGIN
LOCAL i:=1,j:=1,tot:=ΣLIST(L1),E:={};
LOCAL tst:={},res:={},crt=0;
WHILE j<=size(L1) DO
   E:=append(E,Lratio(j)*tot);
   j:=j+1;
END;

WHILE i<=size(L1) DO
   tst:=append(tst,(L1(i)-E(i))^2/E(i));
   i:=i+1;
END;
res:=append(res,ROUND(ΣLIST(tst),4));
crt:=CHISQUARE_ICDF(size(L1)-1,1-a);
res:=append(EVAL(res),"df-ratio given: "+"("+EVAL(size(L1)-1)+","+(1-a)+")"+" tabl=>" + ROUND(crt,4));
res:=append(res,"expt vals "+ E);
return res;
END;

EXPORT ChiBinomial_Pgiven_Oi_Xi_c_p_a(Oi,Xi,c,p,a) //Oi:observerd count, Xi:number of successes.c:total num trials,given p m=0
BEGIN
LOCAL i:=1,k:=1,j:=1,tot:=ΣLIST(Oi);
LOCAL tst:={},res:={},crt:=0,pis:={},Efl:={};
LOCAL expt_ini:={},Ofl:={},Efl:={};
WHILE i<=size(Xi) DO
   //Ei:=BINOMIAL(c,p,Xi(i))*tot;
   expt_ini:=append(expt_ini,BINOMIAL(c,p,Xi(i))*tot); //get intial expected vals list
   pis:=append(pis,Xi(i)+"=" +ROUND(BINOMIAL(c,p,Xi(i)),4));
   //tst:=append(tst,EVAL((Oi(i)-Ei)^2/Ei));
   i:=i+1;
END;
WHILE k<=size(expt_ini) DO //checking if any value less than 5
   IF expt_ini(k) >= 5 
   THEN 
      Efl:=append(Efl,expt_ini(k));
      Ofl:=append(Ofl,Oi(k));
   ELSE
      IF k < size(expt_ini)
      THEN
         expt_ini(k+1):=expt_ini(k+1)+expt_ini(k);
         Oi(k+1):=Oi(k+1)+Oi(k);
      ELSE
         Efl(size(Efl)):=Efl(size(Efl))+E1(k);
         Ofl(size(Ofl)):=Ofl(size(Ofl))+Oi(k);
      END;
   END;
   k:=k+1
END;
WHILE j<=size(Efl) DO
   tst:=append( tst,EVAL( (Ofl(j)-Efl(j))^2/Efl(j) ) );
   j:=j+1;
END;

res:=append(res,"cal:" +ROUND(ΣLIST(tst),4));
crt:=ROUND(CHISQUARE_ICDF(size(Xi)-1,(1-a)),4);
res:=append(res,"df:p given:"+"("+EVAL(size(Xi)-1)+","+(1-a)+")"+" tabl=>" + crt);
res:=append(res,"prbts: "+pis);
res:=append(res,"inti expt vals: "+ expt_ini);
res:=append(res,"final expt vals: "+ Efl);
res:=append(res,"final Oi vals: "+ Ofl);
return res;
END;

EXPORT ChiBinomial_withE_Oi_Xi_E_c_a(Oi,Xi,E,k,a)
BEGIN
LOCAL i:=1,tot:=ΣLIST(Oi);
LOCAL tst:={},res:={},crt:=0,Ei:=0;

WHILE i<=size(Xi) DO
   Ei:=(Oi(i)-E(i))^2/E(i);
   tst:=append(tst,EVAL(Ei));
   i:=i+1;
END;

res:=append(res,ROUND(ΣLIST(tst),4));
crt:=CHISQUARE_ICDF(size(Xi)-1,1-a);
res:=append(res,"df:p given m=0:"+"("+EVAL(size(Xi)-1)+","+(1-a)+")"+" tabl=>" + crt);
return res;
END;

EXPORT ChiBinomialFreq_NoP_fi_xi_c_a(Oi,Xi,c,a)  //Oi:Observered Val,Xi:0,1,2,3,c:number of trails (3 heads?):
BEGIN

LOCAL i:=1,j:=1,k:=1,tot:=ΣLIST(Oi),fx:=ΣLIST(Oi.*Xi);
LOCAL tst:={},res:={},crt:=0;
LOCAL p:=(fx/tot)/c,Efl:={},Ofl:={},E1:={},pis:={}; //Efl final list

WHILE i<=size(Xi) DO  //getting intial E1 list
   E1:=append(E1,ROUND(BINOMIAL(c,p,Xi(i))*tot,3));
   pis:=append(pis,ROUND(BINOMIAL(c,p,Xi(i)),3));
   i:=i+1;
END;

WHILE k<=size(E1) DO //checking if any value less than 5
   IF E1(k) >= 5 
   THEN 
      Efl:=append(Efl,E1(k));
      Ofl:=append(Ofl,Oi(k));
   ELSE
      IF k < size(E1)
      THEN
         E1(k+1):=E1(k+1)+E1(k);
         Oi(k+1):=Oi(k+1)+Oi(k);
      ELSE
         Efl(size(Efl)):=Efl(size(Efl))+E1(k);
         Ofl(size(Ofl)):=Ofl(size(Ofl))+Oi(k);
      END;
   END;
   k:=k+1
END;

WHILE j<=size(Efl) DO
   tst:=append(tst,EVAL( ( Ofl(j)-Efl(j) )^2/Efl(j) ));
   j:=j+1;
END;

res:=append(res,ROUND(ΣLIST(tst),4));
crt:=ROUND(CHISQUARE_ICDF(size(Ofl)-2,1-a),4);
res:=append(res,"df:p not given m=1:"+"("+EVAL(size(Ofl)-2)+","+(1-a)+")"+" tabl=>" + crt);
res:=append(res,"fi*xi: "+fx);
res:=append(res,"Pi vals: " +pis);
res:=append(res,"intial E list: " + E1);
res:=append(res,"final Elist: " +Efl);
res:=append(res,"final Olist: " +Ofl);
return res;
END;

