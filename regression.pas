EXPORT Rcoef_NesL_xy(NesL)
BEGIN
LOCAL n:=size(NesL(1));
LOCAL i:=1,mean_x:=mean(NesL(1)),mean_y:=mean(NesL(2));
LOCAL xi_sum:=ΣLIST(NesL(1)), yi_sum:=ΣLIST(NesL(2));
LOCAL xiyi_sum:=0,sqrxi:=0,sqryi:=0,res:={};
WHILE i <= n DO
    xiyi_sum:= xiyi_sum+ NesL(1,i)*NesL(2,i);
    sqrxi:=sqrxi + NesL(1,i)^2;
    sqryi:=sqryi + NesL(2,i)^2;
    i:= i + 1;
END;
LOCAL R:=(n*xiyi_sum - xi_sum*yi_sum)/sqrt( (n*sqrxi-xi_sum^2)*(n*sqryi-yi_sum^2));
res:=append(res,"R_sample correlation coef: "+ ROUND(R,4) );
res:=append(res,"coefficient determination: "+ (ROUND(R^2,4)) );
res:=append(res,"xi_sum,yi_sum: " + "(" + ROUND(xi_sum,4) + "," + ROUND(yi_sum,4)+")" );
res:=append(res,"sqrxi,sqryi: " + "(" + ROUND(sqrxi,4) + "," + ROUND(sqryi,4)+")");
res:=append(res,"xiyi_sum: " +xiyi_sum);
return res;
END;

EXPORT RegreParam_NesL_a(NesL,alpha)
BEGIN
LOCAL i:=1,mean_x:=mean(NesL(1)),mean_y:=mean(NesL(2));
LOCAL n:=size(NesL(1)), res:={};
LOCAL Sxx:=0, Sxy:=0,Syy:=0,b1:=0,b0:=0;

WHILE i<= n DO 
    Sxx:=Sxx + (NesL(1,i)-mean_x)^2;
    Sxy:=Sxy + ( (NesL(1,i) - mean_x)* (NesL(2,i) - mean_y)  );
    Syy:=Syy + (NesL(2,i)-mean_y)^2;
    i:=i+1;
END;

b1:= ROUND(Sxy/Sxx,4);
b0:=ROUND(mean_y-b1*mean_x,4);

LOCAL k:=1,y_hat:={},SSE:=0;
WHILE k<=n DO
    y_hat:=append(y_hat,b1*NesL(1,k)+b0);
    SSE:=SSE + (NesL(2,k) - (b1*NesL(1,k)+b0))^2;
    k:=k+1;
END;
LOCAL S2b0:= ROUND(SSE/(n-2)*(1/n+mean_x^2/Sxx ),3);
LOCAL S2b1:=ROUND(SSE/(n-2)/Sxx,3),j:=1,y_hat:={};
WHILE j <= size(NesL(1)) DO
   y_hat:=append(y_hat,ROUND(b1*NesL(1,j)+b0,3) );
   j:=j+1;
END;

LOCAL sig:=(1-alpha/2);
res:=append(res,"Ho:Bo=0,T-cal: " + ROUND(b1/sqrt(S2b1),3) );
res:=append(res,"T-tab,sig,df: (" + sig+","+(n-2)+"," + ROUND(STUDENT_ICDF(n-2, ROUND(sig,3)),3)+")");
res:=append(res, "x bar, y bar ("+ ROUND(mean_x,3) +"," + ROUND(mean_y,3)+")"); 
res:=append(res,"Equation: " + "y="+ ROUND(b0,3) + "+" + ROUND(b1,3)+"*x" );
res:=append(res,"Se^2: " + ROUND(SSE/(n-2),3));
res:=append(res,"SSE: " + ROUND(SSE,4));
res:=append(res,"Sx^2: " + ROUND(Sxx/(n-1),3));
res:=append(res,"Sy^2: " + ROUND(Syy/(n-1),3));
res:=append(res,"Sb0^2: " + ROUND(S2b0,3));
res:=append(res,"Sb0: " + ROUND(sqrt(S2b0),3) );
res:=append(res,"Sb1^2: " + ROUND(S2b1,3));
res:=append(res,"Sb1: " + ROUND(sqrt(S2b1),3) );
res:=append(res,"b1: " + ROUND(b1,3));
res:=append(res,"b0: " + ROUND(b0,3));
res:=append(res,"y_hat: " + y_hat);
res:=append(res,"Sxx: " + ROUND(Sxx,3));
res:=append(res,"Sxy: " + ROUND(Sxy,3));
res:=append(res,"Syy: " + ROUND(Syy,3));
return res;
END;

EXPORT CorelationTest_NesL_a(NesL,alpha)
BEGIN
LOCAL n:=size(NesL(1));
LOCAL i:=1,mean_x:=mean(NesL(1)),mean_y:=mean(NesL(2));
LOCAL xi_sum:=ΣLIST(NesL(1)), yi_sum:=ΣLIST(NesL(2));
LOCAL xiyi_sum:=0,sqrxi:=0,sqryi:=0,res:={};
WHILE i <= n DO
    xiyi_sum:= xiyi_sum+ NesL(1,i)*NesL(2,i);
    sqrxi:=sqrxi + NesL(1,i)^2;
    sqryi:=sqryi + NesL(2,i)^2;
    i:= i + 1;
END;
LOCAL R:=(n*xiyi_sum - xi_sum*yi_sum)/sqrt( (n*sqrxi-xi_sum^2)*(n*sqryi-yi_sum^2));
LOCAL tcal:=ROUND(R/sqrt((1-R^2)/(n-2)),4);
LOCAL lefp:=ROUND(STUDENT_ICDF(n-2, (1-alpha/2)),3);
LOCAL rght:=ROUND(STUDENT_ICDF(n-2, (alpha/2)),3);
res:=append(res,"r cofficient: "+ ROUND(R,4));
res:=append(res,"H0: p=0,T-cal: " + tcal);
res:=append(res,"df,a,T-tab: (" + (n-2) + "," + (1-alpha/2) +") " +"("+lefp +","+rght +")");
return res;
END;

EXPORT Interv_r_n_a(smR,n,alpha)
BEGIN
LOCAL bigR:=0.5*LN((1+smR)/(1-smR)),res:={};
LOCAL OR_val:=(1/(n-3))^0.5;
LOCAL uper:=  bigR + NORMALD_ICDF((1-alpha/2))*OR_val;
LOCAL low_val:= bigR - (NORMALD_ICDF((1-alpha/2))*OR_val);
res:=append(res, "UR Interval: (" +ROUND(low_val,3) + "," +ROUND(uper,3) +")" );
res:=append(res, "ρ Interval:" + "z->ρ,-z->-ρ ");
res:=append(res,"r cofficient: "+ ROUND(smR,4));
res:=append(res,"z table: +-" + ROUND(NORMALD_ICDF((1-alpha/2)),3) );
res:=append(res,"R val: " + ROUND(bigR,3));
res:=append(res,"OR val: " + ROUND(OR_val,3));
return res;
END;


